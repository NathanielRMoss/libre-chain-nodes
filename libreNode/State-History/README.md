# State History

Running a State History node is an important aspect of the Libre ecosystem. These nodes can provide chain-state history to apps and wallets - one of the top needed services on any chain. You can also build your own applications that may need state history.

If you are building your own app, then you may want to try to run your own Hyperion node as an API so you can have reliable and custom access to the chain for pushing transactions and getting history.
### Hyperion API Node
* Running a Hyperion Node is a massive help to the Libre ecosystem as all apps and wallets will need API access. 
* Hyperion is a node-based API and chain-history service based on Elasticsearch / Kibana [Hyperion API services](https://github.com/eosrio/hyperion-history-api/tree/v3.3.5) .
  * Hyperion stable version is [v3.3.5](https://github.com/eosrio/hyperion-history-api/tree/v3.3.5) // Nodeos [v2.0.14](https://github.com/eosnetworkfoundation/mandel/releases/tag/v2.0.14)
  * 2TB NVME is recommended if you intend to run a Hyperion node - we also recommend running this on separate hardware than a producer or seed node

# How to Setup Hyperion History API
## Operating System and Hardware Requirements
- Bare metal or a physical server with no virtulization in use (Dedicated hardware)
- Ubuntu 18.04 LTS 
- Fast single-threaded CPU 3.5 Ghz+ (the faster, the better)
- 16GB RAM
- 1TB NVME 
- Ubuntu 18.04 (Recommended) 
- Open TCP Ports (80, 443) on your firewall/router  
- Recommend running nginx reverse proxy in front of your hyperion node for rate limiting and whitelist

## Alternative Setup for Version Compatible with Hyperion

Download 2.0.14 version of Mandel (requires Ubuntu 18.04) using this experimental install script
```
    cd /opt
    git clone https://gitlab.com/libre-chain/libre-chain-nodes
    cd /opt/libre-chain-nodes/libreNode
    ./Scripts/nodesetup.sh
```

Next clone the v3.3.5 branch of Hyperion

```
git clone --branch v3.3.5 https://github.com/eosrio/hyperion-history-api.git
```

Finally, install Hyperion using scripts located here:
```
cd hyperion-history-api
sudo ./install.sh
sudo ./install_env.sh
```

You can use hyp-config to generate the configuration files:
```
./hyp-config new chain libre
```

Check elastic_pass.txt to see if the correct passwords have been set for ampq and elastic in the connections.json
```
cat elastic_pass.txt
nano connections.json
```

Check the chains/libre.config.json and configure the following: 
```
"server_name" = "hyperion.yourdomain.com"
"provider_name" = "yourValidator"
"provider_url" = "https://yourdomain.com"
```
and set the logo for Libre - we have hosted one for you here:

```
"chain_logo_url": "https://i.imgur.com/WKBTNkv.png",
```

Once you have configured connections.json and chains/libre.config.json, you can start the indexer in ABI scan mode by editing the indexer in chains/libre.config.json:

```
  "indexer": {
    "enabled": true,
    "node_max_old_space_size": 4096,
    "start_on": 0,
    "stop_on": 0,
    "rewrite": false,
    "purge_queues": false,
    "live_reader": false,
    "live_only_mode": false,
    "abi_scan_mode": true,
    "fetch_block": true,
    "fetch_traces": true,
    "disable_reading": false,
    "disable_indexing": false,
    "process_deltas": true,
    "disable_delta_rm": true
  },
```

Now run the indexer:

```
./run.sh libre-indexer
```

Let it run until it says no blocks are being processed, then stop it:

```
./stop.sh libre-indexer
```

Then you can start the indexer in live and rewrite mode:

```
  "indexer": {
    "enabled": true,
    "node_max_old_space_size": 4096,
    "start_on": 0,
    "stop_on": 0,
    "rewrite": true,
    "purge_queues": false,
    "live_reader": true,
    "live_only_mode": false,
    "abi_scan_mode": true,
    "fetch_block": true,
    "fetch_traces": true,
    "disable_reading": false,
    "disable_indexing": false,
    "process_deltas": true,
    "disable_delta_rm": true
  },
```

If the indexer is having trouble, then you can disable "live_reader" change the "start_on" and "stop_on" blocks and do 1000000 at a time until you catch up with the chain, then turn the live reader on.

Once that's running you can start the API:

```
./run.sh libre-api
```