# LIBRE CHAIN MAINNET INFO

You will need to begin on the testnet before running on Libre Mainnet. Once you produce blocks for 2 weeks on Testnet, you will be eligible for Mainnet registration.

The genesis.json and peers.ini are here in this directory. You will need to modify your config.ini and run nodeos with these peers to get the blocks.

## Libre Mainnet Links

**Libre Mainnet Telegram**

https://t.me/+M4GpOP_NJYJlZWQx

**Libre Hyperion History API Swagger Docs**  

https://hyperion.libre.rocks/v2/docs 

**Libre Block Explorer**  
                                                                
https://libre-explorer.edenia.cloud/

**Libre Network Monitor**

https://libre.eosio.online
