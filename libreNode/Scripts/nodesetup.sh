#!/bin/bash
# EXPERIMENTAL - TESTED ONLY ONCE
sudo apt update
sudo apt install git jq libtinfo5
sudo chown `whoami` /opt
mkdir -p /opt/eosio/src
cd /opt/eosio/src/
# setup node script for Ubuntu 18.04 and nodeos 2.0.14 --> replace if using Ubuntu 20.04 with the comments below
wget https://apt.eossweden.org/mandel/pool/stable/m/mandel-2014/mandel-2014_2.0.14-1-ubuntu-18.04_amd64.deb
sudo dpkg -i mandel-2014_2.0.14-1-ubuntu-18.04_amd64.deb
# replace above 2 lines with the commented lines at the bottom
cd /opt
git clone https://gitlab.com/libre-chain/libre-chain-nodes
cd /opt/libre-chain-nodes/libreNode
ln -s /usr/opt/mandel/2014-mv/bin/nodeos ./nodeos
ln -s /usr/opt/mandel/2014-mv/bin/cleos ./cleos
ln -s /usr/opt/mandel/2014-mv/bin/keosd ./keosd
sed -i 's/nodeos/\.\/nodeos/g' start.sh
cd /opt/libre-chain-nodes/libreNode/State-History/
sed -i 's/nodeos/\.\/nodeos/g' history-start.sh
echo "type: cd /opt/libre-chain-nodes/libreNode to get to your node directory"


# if you want to use Ubuntu 20.04, then you will need to uncomment and use these lines instead:
# wget https://apt.eossweden.org/mandel/pool/stable/m/mandel-2014/mandel-2014_2.0.14-1-ubuntu-18.04_amd64.deb
# wget https://apt.eossweden.org/mandel/pool/stable/m/mandel-300-rc1/mandel-300-rc1_3.0.0-rc1-ubuntu-18.04_amd64.deb
# wget http://security.ubuntu.com/ubuntu/pool/main/i/icu/libicu60_60.2-3ubuntu3.2_amd64.deb
# dpkg -i libicu60_60.2-3ubuntu3.2_amd64.deb
# dpkg -i mandel-2014_2.0.14-1-ubuntu-18.04_amd64.deb