#!/bin/bash
# Register, stake, and vote
ACCT="node1" #change to your producer name
SIGNKEY="EOS7nUYK3tvvQXJQgGVsQ41TNSwoQXGP5HMTr5wgjiZzypcCzPhv3" # change to your signing key from config.ini
URL="https://mindyourbitcoin.com" # change to your website URL where your bp.json is hosted 
LIBRE_AMOUNT="1000 LIBRE" # if you need tokens on testnet - pls visit the faucet in the readme, on mainnet, try spindrop.libre.org
LOCATION="840" # change to your ISO 3166 Country Code

# Regprod
/opt/libre-chain-nodes/libreNode/cleos.sh system regproducer "$ACCT" "$SIGNKEY" "$URL" $LOCATION -p $ACCT

# Stake
DAYS=`echo $(( $RANDOM % 365 + 1 ))`
/opt/libre-chain-nodes/libreNode/cleos.sh transfer $ACCT stake.libre "$LIBRE_AMOUNT" "stakefor:$DAYS"

# Vote
VOTE='{"voter": "ACCT", "producer": "ACCT"}'
echo $VOTE > VOTE.json
sed -e 's/\"ACCT\"/\"'${ACCT}'"/g' VOTE.json > $ACCT-vote.json 
/opt/libre-chain-nodes/libreNode/cleos.sh push action eosio voteproducer `echo $ACCT-vote.json` -p $ACCT@active
rm VOTE.json
rm $ACCT-vote.json
