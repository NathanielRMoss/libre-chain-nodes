#!/bin/bash
set -e
BLOCKCHAIN="libre"
NETWORK="mainnet"
ACCT="account" #replace with your account name
WALLETNAME="wallet" #replace with your wallet name
PASS="PW44747pass" #replace with your wallet password
DAYS=`echo $(( $RANDOM % 365 + 180 ))` #days to stake for
cleos wallet unlock -n $WALLETNAME --password $PASS > /dev/null 2>&1 || true
#source $BLOCKCHAIN_DIR/$BLOCKCHAIN/$NETWORK/config.node.ini
#source $BLOCKCHAIN_DIR/shared/config.shared.ini
echo "xxx claim $BLOCKCHAIN $NETWORK xxx"
#cd $NODE_DIR
cd ~/libre
 # BP Account
ACCT="quantum"
echo "Claim: $ACCT"
./cleos.sh system claimrewards $ACCT -p $ACCT@claim
sleep 2
BALANCE=`./cleos.sh get currency balance eosio.token $ACCT`
./cleos.sh transfer $ACCT stake.libre "$BALANCE" "stakefor:$DAYS" -p $ACCT@active
./cleos.sh push action eosio voteproducer '{"voter": "'$ACCT'", "producer": "'$ACCT'"}' -p $ACCT@active
